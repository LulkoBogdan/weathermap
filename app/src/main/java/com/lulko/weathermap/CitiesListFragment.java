package com.lulko.weathermap;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lulko.weathermap.interfaces.ICityChooseCallback;
import com.lulko.weathermap.interfaces.ICitySearchListener;
import com.lulko.weathermap.models.City;

import java.util.ArrayList;

/**
 * Created by Богдан on 29.05.2016.
 */
public class CitiesListFragment extends Fragment implements ICitySearchListener{

    private ArrayList<City> cities = new ArrayList<>();
    private CitiesAdapter adapter;
    private ICityChooseCallback cityChooseCallback;

    public static CitiesListFragment getInstance(ICityChooseCallback cityChooseCallback) {
        CitiesListFragment fragment = new CitiesListFragment();
        fragment.setCityChooseCallback(cityChooseCallback);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_cities_list, container, false);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewCities);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new CitiesAdapter(cities, cityChooseCallback);
        recyclerView.setAdapter(adapter);
        return rootView;
    }

    @Override
    public void onCitiesFound(ArrayList<City> cities) {
        this.cities.clear();
        this.cities.addAll(cities);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void clearResults() {
        this.cities.clear();
    }

    public void setCityChooseCallback(ICityChooseCallback cityChooseCallback) {
        this.cityChooseCallback = cityChooseCallback;
    }
}
