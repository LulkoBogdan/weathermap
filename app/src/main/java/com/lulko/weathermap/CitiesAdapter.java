package com.lulko.weathermap;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lulko.weathermap.interfaces.ICityChooseCallback;
import com.lulko.weathermap.models.City;

import java.util.ArrayList;

/**
 * Created by Богдан on 29.05.2016.
 */
public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.ViewHolder> {

    private ArrayList<City> cities;
    private ICityChooseCallback cityChooseCallback;

    public CitiesAdapter(ArrayList<City> cities, ICityChooseCallback cityChooseCallback) {
        this.cities = cities;
        this.cityChooseCallback = cityChooseCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_city, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        City city = cities.get(position);
        holder.tvAdditionalInfo.setText(city.getCountry() + ", " + city.getAdminName());
        holder.tvCityName.setText(city.getName());
    }

    @Override
    public int getItemCount() {
        return cities.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        LinearLayout llContainer;
        TextView tvCityName;
        TextView tvAdditionalInfo;

        public ViewHolder(View itemView) {
            super(itemView);
            llContainer = (LinearLayout) itemView.findViewById(R.id.llContainer);
            tvCityName = (TextView) itemView.findViewById(R.id.textViewCityName);
            tvAdditionalInfo = (TextView) itemView.findViewById(R.id.textViewAdditionalInfo);
            llContainer.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.llContainer:
                    if (cityChooseCallback != null)
                        cityChooseCallback.cityChoose(cities.get(getAdapterPosition()));
                    break;
            }
        }
    }
}
