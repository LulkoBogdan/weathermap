package com.lulko.weathermap.helpers;

import android.content.Context;
import android.net.Uri;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.lulko.weathermap.R;
import com.lulko.weathermap.interfaces.ICitySearchListener;
import com.lulko.weathermap.interfaces.IWeatherListener;
import com.lulko.weathermap.models.City;
import com.lulko.weathermap.models.WeatherItem;
import com.lulko.weathermap.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Богдан on 27.05.2016.
 */
public class RequestHelper implements Response.ErrorListener, Response.Listener<JSONObject>{

    private final static String USER_ID = "3922b95b3b1b70ff468853c023a0ece2";
    private final static String URL_GET_WEATHER_BY_COORDINATE = "http://api.openweathermap.org/data/2.5/weather?";
    private final static String URL_GET_WEATHER_BY_CITY_NAME = "http://api.openweathermap.org/data/2.5/weather?q=";
    public static final String CITIES = "http://api.geonames.org/search?name=%1$s&lang=%2$s&username=%3$s&featureClass=P&type=json&maxRows=10";

    private Context context;
    private RequestQueue requestQueue;
    private IWeatherListener weatherListener;
    private ICitySearchListener citySearchListener;
    private JsonObjectRequest citySearchRequest;

    public RequestHelper(Context context, IWeatherListener weatherListener) {
        this.context = context;
        this.weatherListener = weatherListener;
        requestQueue = Volley.newRequestQueue(context);
    }

    public void clear() {
        requestQueue.stop();
    }

    private String getAppId() {
        return "&units=metric&APPID=" + USER_ID;
    }

    private String coordinatesToString(double longitude, double latitude) {
        return "lat=" + latitude + "&lon=" + longitude;
    }

    public void getWeatherByCoordinates(double longitude, double latitude) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                URL_GET_WEATHER_BY_COORDINATE + coordinatesToString(longitude, latitude) + getAppId(), null,
                this, this);
        requestQueue.add(jsonObjectRequest);
    }

    public void getWeatherByCityName(String name) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                URL_GET_WEATHER_BY_CITY_NAME + name + getAppId(), null, this, this);
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Utils.log(error.getMessage());
        Toast.makeText(context, R.string.message_error_on_request, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(JSONObject response) {
        Utils.log(response.toString());
        WeatherItem weatherItem = JsonHelper.parseWeatherItem(response);
        if (weatherItem == null) {
            Toast.makeText(context, R.string.message_unexpected_response, Toast.LENGTH_SHORT).show();
        }
        if (weatherListener != null)
            weatherListener.onWeatherGet(weatherItem);
    }

    public void getCityName(String cityName, ICitySearchListener citySearchListener) {
        if (citySearchRequest != null)
            citySearchRequest.cancel();
        this.citySearchListener = citySearchListener;
        String city = Uri.encode(cityName);
        final String url = String.format(CITIES, city, Locale.ENGLISH.getLanguage().toLowerCase(), "BogdanLulko");
        citySearchRequest = new JsonObjectRequest(Request.Method.GET,
                url, null, new CitySearchListener(), this);
        requestQueue.add(citySearchRequest);
    }

    private class CitySearchListener implements Response.Listener<JSONObject> {

        @Override
        public void onResponse(JSONObject response) {
            Utils.log(response.toString());
            ArrayList<City> cities = JsonHelper.parseCitiesList(response);
            citySearchListener.onCitiesFound(cities);
        }
    }
}
