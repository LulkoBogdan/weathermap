package com.lulko.weathermap.helpers;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.lulko.weathermap.R;
import com.lulko.weathermap.interfaces.ILocationListener;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Богдан on 27.05.2016.
 */
public class LocationHelper {

    private final static int LOCATION_UPDATE_PERIOD = 10 * 1000; // 10 seconds

    private boolean gps_enabled = false;
    private boolean network_enabled = false;

    private Timer mTimer;
    private ILocationListener locationListener;
    private LocationManager locationManager;
    private Context context;

    public LocationHelper(ILocationListener locationListener, Context context) {
        this.locationListener = locationListener;
        this.context = context;
    }

    public boolean getLocation() {
        if (!checkPermission()) {
            Toast.makeText(context, R.string.message_permission_not_granted, Toast.LENGTH_SHORT).show();
            return false;
        }
        if(locationManager == null)
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        try{
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        try{
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        if(!gps_enabled && !network_enabled)
            return false;
        if(gps_enabled)
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerGps);
        if(network_enabled)
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerNetwork);
        mTimer = new Timer();
        mTimer.schedule(new GetLastLocation(), 0, LOCATION_UPDATE_PERIOD);
        return true;
    }

    public void clear() {
        if (mTimer != null) {
            mTimer.cancel();
        }
        if (locationManager != null && checkPermission()) {
            locationManager.removeUpdates(locationListenerGps);
            locationManager.removeUpdates(locationListenerNetwork);
        }
    }

    private LocationListener locationListenerGps = new LocationListener() {

        public void onLocationChanged(Location location) {
            locationListener.gotLocation(location);
        }

        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    private LocationListener locationListenerNetwork = new LocationListener() {

        public void onLocationChanged(Location location) {
            locationListener.gotLocation(location);
        }

        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    private boolean checkPermission() {
        int permissionCheckCoarse = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION);
        int permissionCheckFine = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionCheckCoarse == PackageManager.PERMISSION_GRANTED && permissionCheckFine == PackageManager.PERMISSION_GRANTED;
    }

    private class GetLastLocation extends TimerTask {
        @Override
        public void run() {
            if (!checkPermission()) {
                Toast.makeText(context, R.string.message_permission_not_granted, Toast.LENGTH_SHORT).show();
            }
            Location net_loc = null, gps_loc = null;
            if(gps_enabled)
                gps_loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if(network_enabled)
                net_loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if(gps_loc != null && net_loc != null){
                if(gps_loc.getTime() > net_loc.getTime())
                    locationListener.gotLocation(gps_loc);
                else
                    locationListener.gotLocation(net_loc);
                return;
            }
            if(gps_loc != null){
                locationListener.gotLocation(gps_loc);
                return;
            }
            if(net_loc != null){
                locationListener.gotLocation(net_loc);
                return;
            }
            locationListener.gotLocation(null);
        }
    }
}
