package com.lulko.weathermap.helpers;

import android.location.Location;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Богдан on 29.05.2016.
 */
public class MapHelper implements GoogleMap.OnMapLongClickListener, OnMapReadyCallback {

    private Marker marker;
    private LatLng markerLatLng;
    private GoogleMap googleMap;
    private Button btnCurrentPosition;
    private RequestHelper requestHelper;
    private LocationHelper locationHelper;

    public MapHelper(Button btnCurrentPosition, RequestHelper requestHelper, LocationHelper locationHelper) {
        this.btnCurrentPosition = btnCurrentPosition;
        this.requestHelper = requestHelper;
        this.locationHelper = locationHelper;
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        locationHelper.clear();
        markerLatLng = latLng;
        resetMarker();
        setMarker();
        btnCurrentPosition.setEnabled(true);
        requestHelper.getWeatherByCoordinates(latLng.longitude, latLng.latitude);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMapLongClickListener(this);
        setMarker();
    }

    public void setCurrentLocation(Location location) {
        locationToLatLng(location);
        setMarker();
//        moveCameraToMarker();
    }

    public void setMapFragment(MapFragment mapFragment) {
        mapFragment.getMapAsync(this);
    }

    public void resetMarker() {
        if (marker != null) {
            marker.remove();
            marker = null;
        }
    }

    private void setMarker() {
        if (googleMap == null || markerLatLng == null)
            return;
        if (marker != null) {
            marker.setPosition(markerLatLng);
            marker.setTitle(getLocationString());
            return;
        }
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(markerLatLng);
        marker = googleMap.addMarker(markerOptions);
        moveCameraToMarker();
    }

    private void locationToLatLng(Location location) {
        markerLatLng = new LatLng(location.getLatitude(), location.getLongitude());
    }

    private String getLocationString() {
        return "Lat = " + markerLatLng.latitude + "\nLon = " + markerLatLng.longitude;
    }

    private void moveCameraToMarker() {
        CameraPosition markerPosition = new CameraPosition.Builder().target(markerLatLng)
                        .zoom(15.5f)
                        .bearing(0)
                        .tilt(25)
                        .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(markerPosition));
    }
}
