package com.lulko.weathermap.helpers;

import com.lulko.weathermap.models.City;
import com.lulko.weathermap.models.WeatherItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Богдан on 28.05.2016.
 */
public class JsonHelper {

    public static WeatherItem parseWeatherItem(JSONObject jsonObject) {
        WeatherItem item = new WeatherItem();
        try {
            JSONObject coordObject = jsonObject.getJSONObject("coord");
            parseCoord(item, coordObject);
            JSONArray weatherObject = jsonObject.getJSONArray("weather");
            parseWeather(item, weatherObject);
            item.setBase(jsonObject.getString("base"));
            JSONObject mainObject = jsonObject.getJSONObject("main");
            parseMain(item, mainObject);
            JSONObject windObject = jsonObject.getJSONObject("wind");
            parseWind(item, windObject);
            JSONObject cloudsObject = jsonObject.getJSONObject("clouds");
            item.setClouds(cloudsObject.getInt("all"));
            item.setDate(jsonObject.getLong("dt"));
            JSONObject sysObject = jsonObject.getJSONObject("sys");
            parseSys(item, sysObject);
            item.setId(jsonObject.getInt("id"));
            item.setCity(jsonObject.getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
            item = null;
        }
        return item;
    }

    private static void parseCoord(WeatherItem item, JSONObject jsonObject) throws JSONException {
        item.setLatitude(jsonObject.getDouble("lat"));
        item.setLongitude(jsonObject.getDouble("lon"));
    }

    private static void parseWeather(WeatherItem item, JSONArray jsonArray) throws JSONException {
        JSONObject jsonObject;
        for (int i = 0; i < jsonArray.length(); i++) {
            jsonObject = jsonArray.getJSONObject(i);
            item.setWeatherId(jsonObject.getInt("id"));
            item.setMain(jsonObject.getString("main"));
            item.setDescription(jsonObject.getString("description"));
            item.setIcon(jsonObject.getString("icon"));
        }
    }

    private static void parseMain(WeatherItem item, JSONObject jsonObject) throws JSONException {
        item.setTemperature(jsonObject.getDouble("temp"));
        item.setPressure(jsonObject.getInt("pressure"));
        item.setHumidity(jsonObject.getInt("humidity"));
        item.setTemperatureMin(jsonObject.getDouble("temp_min"));
        item.setTemperatureMax(jsonObject.getDouble("temp_max"));
    }

    private static void parseWind(WeatherItem item, JSONObject jsonObject) throws JSONException {
        item.setWindSpeed(jsonObject.getInt("speed"));
        item.setWindDegree(jsonObject.getInt("deg"));
    }

    private static void parseSys(WeatherItem item, JSONObject jsonObject) throws JSONException {
        item.setCountry(jsonObject.getString("country"));
        item.setSunrise(jsonObject.getLong("sunrise"));
        item.setSunset(jsonObject.getLong("sunset"));
    }

    public static ArrayList<City> parseCitiesList(JSONObject jsonObject) {
        ArrayList<City> cities = new ArrayList<>();
        City city;
        JSONObject jsonItem;
        try {
            JSONArray jsonCities = jsonObject.getJSONArray("geonames");
            for (int i = 0; i < jsonCities.length(); i++) {
                jsonItem = jsonCities.getJSONObject(i);
                city = new City();
                city.setAdminName(jsonItem.getString("adminName1"));
                city.setCountry(jsonItem.getString("countryName"));
                city.setLatitude(Double.parseDouble(jsonItem.getString("lat")));
                city.setLongitude(Double.parseDouble(jsonItem.getString("lng")));
                city.setName(jsonItem.getString("name"));
                cities.add(city);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            cities.clear();
        }
        return cities;
    }

}
