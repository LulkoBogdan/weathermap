package com.lulko.weathermap.interfaces;

/**
 * Created by Богдан on 29.05.2016.
 */
public interface ISearchContainerActions {
    void openSearchResultsList();
    void closeSearchResultsList();
}
