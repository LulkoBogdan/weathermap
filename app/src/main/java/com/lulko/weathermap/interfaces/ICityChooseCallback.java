package com.lulko.weathermap.interfaces;

import com.lulko.weathermap.models.City;

/**
 * Created by Богдан on 29.05.2016.
 */
public interface ICityChooseCallback {
    void cityChoose(City city);
}
