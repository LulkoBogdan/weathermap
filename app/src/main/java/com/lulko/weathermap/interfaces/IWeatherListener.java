package com.lulko.weathermap.interfaces;

import com.lulko.weathermap.models.WeatherItem;

/**
 * Created by Богдан on 29.05.2016.
 */
public interface IWeatherListener {
    void onWeatherGet(WeatherItem weatherItem);
}
