package com.lulko.weathermap.interfaces;

import com.lulko.weathermap.models.City;

import java.util.ArrayList;

/**
 * Created by Богдан on 29.05.2016.
 */
public interface ICitySearchListener {
    void onCitiesFound(ArrayList<City> cities);
}
