package com.lulko.weathermap.interfaces;

import android.location.Location;

/**
 * Created by Богдан on 28.05.2016.
 */
public interface ILocationListener {
    void gotLocation(Location location);
}
