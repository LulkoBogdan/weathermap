package com.lulko.weathermap;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.lulko.weathermap.helpers.LocationHelper;
import com.lulko.weathermap.helpers.MapHelper;
import com.lulko.weathermap.helpers.RequestHelper;
import com.lulko.weathermap.interfaces.ICityChooseCallback;
import com.lulko.weathermap.interfaces.ICitySearchListener;
import com.lulko.weathermap.interfaces.ILocationListener;
import com.lulko.weathermap.interfaces.ISearchContainerActions;
import com.lulko.weathermap.interfaces.IWeatherListener;
import com.lulko.weathermap.models.City;
import com.lulko.weathermap.models.WeatherItem;
import com.lulko.weathermap.utils.Utils;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements ILocationListener, View.OnClickListener,
        IWeatherListener, ICitySearchListener, ISearchContainerActions, ICityChooseCallback{

    private final int PERMISSION_REQUEST_LOCATION = 100;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private Button btnCurrentLocation;
    private EditText etSearch;
    private TextView tvWeather;
    private RelativeLayout rlWeatherContainer;
    private FrameLayout flSearchResultContainer;
    private CitiesListFragment citiesListFragment;

    private RequestHelper requestHelper;
    private LocationHelper locationHelper;
    private MapHelper mapHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnCurrentLocation = (Button) findViewById(R.id.buttonCurrentPosition);
        if (btnCurrentLocation != null) {
            btnCurrentLocation.setOnClickListener(this);
        }
        rlWeatherContainer = (RelativeLayout) findViewById(R.id.rlWeatherContainer);
        tvWeather = (TextView) findViewById(R.id.textViewWeather);
        etSearch = (EditText) findViewById(R.id.editTextCitySearch);
        flSearchResultContainer = (FrameLayout) findViewById(R.id.flSearchResultsContainer);
        setClickListenerToView(R.id.imageViewClose);
        setClickListenerToView(R.id.imageViewSearch);
        requestHelper = new RequestHelper(this, this);
        locationHelper = new LocationHelper(this, this);
        mapHelper = new MapHelper(btnCurrentLocation, requestHelper, locationHelper);
        etSearch.addTextChangedListener(new TextChangeListener(requestHelper, this, this));
        addMapFragment();
        addSearchResultFragment();
        checkAll();
        checkPlayServices();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        requestHelper.clear();
        locationHelper.clear();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_LOCATION) {
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, R.string.message_permission_not_granted, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void gotLocation(final Location location) {
        if (btnCurrentLocation.isEnabled())
            return;
        Utils.log("lat = " + location.getLatitude() + "   lon = " + location.getLongitude());
        requestHelper.getWeatherByCoordinates(location.getLongitude(), location.getLatitude());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mapHelper.setCurrentLocation(location);
            }
        });
    }

    private void checkAll() {
        checkPermissions();
        checkConnection();
        checkGPS();
    }

    private void addSearchResultFragment() {
        citiesListFragment = CitiesListFragment.getInstance(this);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.flSearchResultsContainer, citiesListFragment);
        fragmentTransaction.commit();
    }

    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS)) {
                Toast.makeText(this, R.string.message_explain_permissions, Toast.LENGTH_LONG).show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_REQUEST_LOCATION);
            }
        }
    }

    private void checkGPS() {
        LocationManager manager = (LocationManager) getSystemService( LOCATION_SERVICE );
        if ( !manager.isProviderEnabled(LocationManager.GPS_PROVIDER) ) {
            Toast.makeText(this, R.string.message_gps_is_off, Toast.LENGTH_SHORT).show();
        }
    }

    private void checkConnection() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            Toast.makeText(this, R.string.message_no_connection, Toast.LENGTH_SHORT).show();
        }
    }

    private void setClickListenerToView(int id) {
        View close = findViewById(id);
        if (close != null)
            close.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonCurrentPosition:
                v.setEnabled(false);
                resetWeatherView();
                locationHelper.getLocation();
                mapHelper.resetMarker();
                break;
            case R.id.imageViewClose:
                resetWeatherView();
                mapHelper.resetMarker();
                btnCurrentLocation.setEnabled(true);
                locationHelper.clear();
                break;
            case R.id.imageViewSearch:
                String city = etSearch.getText().toString();
                if (TextUtils.isEmpty(city))
                    break;
                requestHelper.getCityName(city, this);
                openSearchResultsList();
                break;
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(this, R.string.message_trouble_with_play_services, Toast.LENGTH_SHORT).show();
                finish();
            }
            return false;
        }
        return true;
    }

    private void addMapFragment() {
        MapFragment mMapFragment = MapFragment.newInstance();
        mapHelper.setMapFragment(mMapFragment);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.map_fragment_container, mMapFragment);
        fragmentTransaction.commit();
    }

    private void resetWeatherView() {
        tvWeather.setText("");
        rlWeatherContainer.setVisibility(View.GONE);
    }

    @Override
    public void openSearchResultsList() {
        flSearchResultContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void closeSearchResultsList() {
        flSearchResultContainer.setVisibility(View.GONE);
        citiesListFragment.clearResults();
    }

    @Override
    public void onWeatherGet(WeatherItem weatherItem) {
        if (tvWeather != null && rlWeatherContainer != null && weatherItem != null) {
            tvWeather.setText(weatherItem.toString());
            rlWeatherContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCitiesFound(ArrayList<City> cities) {
        citiesListFragment.onCitiesFound(cities);
    }

    @Override
    public void cityChoose(City city) {
        hideKeyBoard();
        resetWeatherView();
        closeSearchResultsList();
        btnCurrentLocation.setEnabled(true);
        resetWeatherView();
        locationHelper.clear();
        requestHelper.getWeatherByCityName(city.getName());
        mapHelper.onMapLongClick(new LatLng(city.getLatitude(), city.getLongitude()));
    }

    public void hideKeyBoard(){
        InputMethodManager inputMethodManager = (InputMethodManager)  getSystemService(Activity.INPUT_METHOD_SERVICE);
        if(inputMethodManager != null && getCurrentFocus() != null){
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private static class TextChangeListener implements TextWatcher {

        private RequestHelper requestHelper;
        private ICitySearchListener citySearchListener;
        private ISearchContainerActions searchContainerActions;

        public TextChangeListener(RequestHelper requestHelper, ICitySearchListener citySearchListener, ISearchContainerActions searchContainerActions) {
            this.requestHelper = requestHelper;
            this.citySearchListener = citySearchListener;
            this.searchContainerActions = searchContainerActions;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {}

        @Override
        public void afterTextChanged(Editable s) {
            String city = s.toString();
            if (TextUtils.isEmpty(city)) {
                searchContainerActions.closeSearchResultsList();
                return;
            }
            searchContainerActions.openSearchResultsList();
            requestHelper.getCityName(s.toString(), citySearchListener);
        }
    }

    @Override
    public void onBackPressed() {
        if (flSearchResultContainer.getVisibility() == View.VISIBLE) {
            closeSearchResultsList();
            return;
        }
        super.onBackPressed();
    }
}
