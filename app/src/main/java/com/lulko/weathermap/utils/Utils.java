package com.lulko.weathermap.utils;

import android.text.TextUtils;
import android.util.Log;

/**
 * Created by Богдан on 27.05.2016.
 */
public class Utils {

    public static boolean DEBUG = true;
    private static final String TAG = "OPTECK PRO";

    public static void log(String msg){
        if (DEBUG){
            Log.d(TAG, getLocation() + msg);
        }
    }

    private static String getLocation() {
        final String className = Utils.class.getName();
        final StackTraceElement[] traces = Thread.currentThread().getStackTrace();
        boolean found = false;
        for (StackTraceElement trace : traces) {
            try {
                if (found) {
                    if (!trace.getClassName().startsWith(className)) {
                        Class<?> clazz = Class.forName(trace.getClassName());
                        return "[" + getClassName(clazz) + ":" + trace.getMethodName() + ":" + trace.getLineNumber() + "]: ";
                    }
                } else if (trace.getClassName().startsWith(className)) {
                    found = true;
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return "[]: ";
    }

    private static String getClassName(Class<?> clazz) {
        if (clazz != null) {
            if (!TextUtils.isEmpty(clazz.getSimpleName())) {
                return clazz.getSimpleName();
            }
            return getClassName(clazz.getEnclosingClass());
        }
        return "";
    }

}
